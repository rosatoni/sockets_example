package sslsockets_example;

import java.io.*;
import javax.net.ssl.*;

public class SSLServer {
    public static void main(String args[]) throws Exception
    {
        try{

        SSLServerSocketFactory factory=(SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
        SSLServerSocket sslserversocket=(SSLServerSocket) factory.createServerSocket(1234);

        SSLSocket sslsocket=(SSLSocket) sslserversocket.accept();
        
        DataInputStream is=new DataInputStream(sslsocket.getInputStream());
        PrintStream os=new PrintStream(sslsocket.getOutputStream());
        while(true)  
        {

            String input=is.readUTF();
            String istring=input.toUpperCase();

            os.println(istring);
        }
        }
        catch(IOException e)
        {
           System.out.print(e);
        }
    }
}